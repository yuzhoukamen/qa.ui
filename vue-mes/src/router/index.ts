import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('../views/LoginView.vue')
        },
        {
            path: '/about',
            name: 'about',
            component: () => import('../views/AboutView.vue')
        },
        {
            path: '/index',
            name: 'index',
            component: () => import('../views/IndexView.vue')
        }
    ],
})

router.beforeEach((to, from, next) => {
    console.log('Navigating to:', from.path, to.path);
    next()
})

export default router
