//import './assets/main.css'

// 全局样式
import "@/assets/css/global.scss";

import {createApp} from 'vue'
import {createPinia} from 'pinia'

import App from './App.vue'
import router from './router'

import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import en from 'element-plus/es/locale/lang/en'
import 'element-plus/dist/index.css'

// Register icon sprite
import "virtual:svg-icons-register";

import * as echarts from 'echarts';

import axios from 'axios'

// 全局配置 Axios
axios.defaults.baseURL = process.url;
axios.defaults.timeout = 5000;
//axios.defaults.headers.common['Authorization'] = 'Bearer your_token';

// 请求拦截器
axios.interceptors.request.use(
    config => {
        config.headers['Content-Type'] = 'application/json;charset=utf-8';

        const token = localStorage.getItem('token');
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    response => {
        try {
            let res = response.data;
            // 如果是返回的文件
            if (response.config.responseType === 'blob') {
                return res
            }
            // 兼容服务端返回的字符串数据
            if (typeof res === 'string') {
                res = res ? JSON.parse(res) : res
            }

            // console.log(response)

            return response;
        } catch (error) {
            console.log(error)
        }
    },
    error => {
        console.log('' + error) // for debug
        return Promise.reject(error)
    }
);

const app = createApp(App)

app.config.globalProperties.$axios = axios;
app.provide('$echarts', echarts)

app.use(createPinia())
app.use(router)
app.use(ElementPlus, {
    locale: zhCn
})

app.mount('#app')
