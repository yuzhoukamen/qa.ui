/**
 * Format date to string
 * 格式化日期为字符串 yyyy-MM-dd HH:mm:ss
 * @param time
 */
export function toQaDateTime(time: string | number) {
    if (time === null) {
        return ''
    } else {
        const date = new Date(time)
        const y = date.getFullYear()
        let m: string | number = date.getMonth() + 1
        m = m < 10 ? `0${String(m)}` : m
        let d: string | number = date.getDate()
        d = d < 10 ? `0${String(d)}` : d
        let h: string | number = date.getHours()
        h = h < 10 ? `0${String(h)}` : h
        let minute: string | number = date.getMinutes()
        minute = minute < 10 ? `0${String(minute)}` : minute
        let second: string | number = date.getSeconds()
        second = second < 10 ? `0${String(second)}` : second
        return `${String(y)}-${String(m)}-${String(d)}   ${String(h)}:${String(
            minute
        )}:${String(second)}`
    }
}

/**
 * Format date to year
 * 格式化日期为年份 yyyy
 * @param date
 */
export function toYear(date: Date) {
    return date.getFullYear();
}

/**
 * Format date to month
 * 格式化日期为月份 MM
 * @param date
 */
export function toMonth(date: Date) {
    return (date.getMonth() + 1).toString().padStart(2, '0');
}

/**
 * Format date to day
 * 格式化日期为日份 dd
 * @param date
 */
export function toDay(date: Date) {
    return (date.getDate()).toString().padStart(2, '0');
}

/**
 * Format date to string
 * 格式化日期为字符串 yyyy-MM-dd
 * @param date
 */
export function toDate(date: Date) {
    return `${toYear(date)}-${toMonth(date)}-${toDay(date)}`;
}

/**
 * Format date to time string
 * 格式化日期为时间字符串 HH:mm:ss
 * @param date
 */
export function toTime(date: Date) {
    return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

/**
 * Get current date and time in QA format
 */
export function now() {
    return toQaDateTime(new Date().toLocaleString());
}