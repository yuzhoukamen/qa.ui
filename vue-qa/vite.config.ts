import {fileURLToPath, URL} from 'node:url'

import {defineConfig, loadEnv} from 'vite'
import vue from '@vitejs/plugin-vue'
import {createSvgIconsPlugin} from "vite-plugin-svg-icons";
import path from 'path';
import fs from 'fs';

// https://vitejs.dev/config/
export default defineConfig(({command, mode}) => {
    const env = loadEnv(mode, process.cwd(), '');

    let qaConfig = "1";

    const status = fs.statSync("public/config.qa");

    if (status.isFile()) {
        qaConfig = fs.readFileSync("public/config.qa", "utf-8")
    }

    console.log(qaConfig);

    let url = env.VITE_API_URL;

    switch (qaConfig) {
        case "1":
            url = env.VITE_API_URL;
            break;
        case "2":
            url = env.VITE_API_URL_TWO;

            break;
    }

    //console.log(env)
    console.log(url)

    return {
        plugins: [
            vue(),
            createSvgIconsPlugin({
                iconDirs: [path.resolve(__dirname, "./src/assets/iconsvg")],
                symbolId: "icon-[name]",
            }),
        ],
        root: process.cwd(),

        resolve: {
            alias: {
                '@': fileURLToPath(new URL('./src', import.meta.url))
            }
        },
        server: {
            host: '0.0.0.0',
            port: env.VITE_PORT as unknown as number,

        },
        // vite 配置
        define: {
            __APP_ENV__: JSON.stringify(env.APP_ENV),

            'process.env': env,
            //'process.url': url,
            "process.url": JSON.stringify(url),
        },

    }
})