import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('../views/LoginView.vue')
        }
        ,
        {
            path: '/about',
            name: 'about',
            component: () => import('../views/AboutView.vue')
        },
        {
            path: '/cc-board',
            name: 'cc-board',
            component: () => import('../views/mcc/BoardView.vue')
        },
        {
            path: '/guide',
            name: 'guide',
            component: () => import('../views/qa/GuideView.vue')
        }
    ]
})

export default router
