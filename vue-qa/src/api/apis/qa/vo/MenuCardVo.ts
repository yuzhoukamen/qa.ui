export interface MenuCardVo {
    name: string,
    menus: MenuCardUrlVo[]
}

export interface MenuCardUrlVo {
    name: string,
    url: string,
}