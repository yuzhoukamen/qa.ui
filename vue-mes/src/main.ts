import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
//import ElementPlus from 'element-plus'
//import 'element-plus/dist/index.css'
import axios from 'axios'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

/*
import 'v3-easyui/dist/themes/default/easyui.css';
import 'v3-easyui/dist/themes/icon.css';
import 'v3-easyui/dist/themes/vue.css';

import EasyUI from 'v3-easyui';
import locale from 'v3-easyui/dist/locale/easyui-lang-en'
*/

import * as echarts from 'echarts';

const app = createApp(App)

// 注册 Element Plus 图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

app.provide('$echarts', echarts)

// 全局配置 Axios
axios.defaults.baseURL = 'http://10.15.3.80:9004/api';
axios.defaults.timeout = 5000;
//axios.defaults.headers.common['Authorization'] = 'Bearer your_token';

app.config.globalProperties.$axios = axios;

// 请求拦截器
axios.interceptors.request.use(
    config => {
        config.headers['Content-Type'] = 'application/json;charset=utf-8';

        const token = localStorage.getItem('token');
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    response => {
        try {
            let res = response.data;
            // 如果是返回的文件
            if (response.config.responseType === 'blob') {
                return res
            }
            // 兼容服务端返回的字符串数据
            if (typeof res === 'string') {
                res = res ? JSON.parse(res) : res
            }

            // console.log(response)

            return response;
        } catch (error) {
            console.log(error)
        }
    },
    error => {
        console.log('' + error) // for debug
        return Promise.reject(error)
    }
);

app.use(router)
//app.use(ElementPlus)// 注册ElementPlus
/*
app.use(EasyUI, {
    locale: locale
}) // 注册EasyUI
*/

app.mount('#app')
