import {get} from "@/utils/request";
import type {MenuCardVo} from "@/api/apis/qa/vo/MenuCardVo";

/**
 * 获取菜单卡片
 */
export const menuCardGetList = async () => {
    return (await get('/qa/menu-card-get-list')).data as MenuCardVo[];
};