import {get, post, sendError, sendSuccess} from "@/utils/request";
import type {MachineStatusClassVo} from "@/api/apis/cc/vo/MachineStatusClassVo";

/**
 * 获取看板数据
 */
export const boardInfo = async () => {
    return (await get('/cc-data/machine-status-board-get-list')).data as MachineStatusClassVo;
};