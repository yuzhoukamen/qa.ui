import axios from "axios";
import {get} from "@/utils/request";

/**
 * 获取版权信息
 */
export const copyRight = async () => {
    return (await get('/qa/dic-info-by-code-get-model/100.001.003')).data.result;
};

/**
 * 获取单位信息
 * @constructor
 */
export const unitInfo = async () => {
    return (await get('/qa/dic-info-by-code-get-model/100.001')).data.result;
};