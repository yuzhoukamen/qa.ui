import axios from 'axios'
import {ElMessage} from "element-plus";

export interface ApiResponse {
    data: any;
    errors: string,
    extras: any,
    statusCode: number,
    succeeded: boolean;
    timestamp: number;
}

/**
 * GET request
 * 封装的axios.get方法
 * @param url
 */
export const get = async (url: string) => {
    const response = await axios.get(url);

    return response.data as ApiResponse;
};

/**
 * POST request
 * 封装的axios.post方法
 * @param url
 * @param data
 */
export const post = async (url: string, data: any) => {
    const response = await axios.post(url, data);

    return response.data as ApiResponse;
};

/**
 * 发送消息提示
 * @param msg
 */
export const sendSuccess = (msg: string) => {
    ElMessage.success(msg);
}

/**
 * 发送错误消息提示
 * @param msg
 */
export const sendError = (msg: string) => {
    ElMessage.error(msg);
}